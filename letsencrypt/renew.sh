#!/bin/sh

SITE=gtg.grit.systems

# move to the correct let's encrypt directory
cd /etc/letsencrypt/live/$SITE

# cat files to make combined .pem for haproxy
cat fullchain.pem privkey.pem > /etc/haproxy/certs/$SITE.pem
cp /etc/haproxy/certs/$SITE.pem /root/utils/haproxy_frontend_config/private/$SITE.pem

# reload haproxy
cd /root/utils/haproxy_frontend_config
docker-compose -f production.yml up --build -d haproxy
