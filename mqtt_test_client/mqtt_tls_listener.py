#!/usr/bin/python2
import paho.mqtt.client as mqtt
import ssl

Topic = "grit_sub"


'''
on_message() is a callback function that is called every time a message is received

Your implementation to handle the received message should be inside on_message()

'''

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe(Topic, qos=1)

def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))


def on_message(client, userdata, msg):
    # print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
    global count
    count += 1

    print(str(msg.payload) + " " + str(count) + "\n")    

#test_host = "iot.eclipse.org"
remote_host = "mqtt.grit.systems"
host = "localhost"
ssl_port = 8883

count = 0

client = mqtt.Client("listener_client")
client.on_connect = on_connect
client.on_subscribe = on_subscribe
client.on_message = on_message

print ("Setting TLS")

ca_cert = "ca_certificate.pem"

client.tls_set(
        ca_certs=ca_cert,
        cert_reqs=ssl.CERT_NONE,
        tls_version=ssl.PROTOCOL_TLSv1_2)
#client.tls_insecure_set(True)

client.connect(host=remote_host, port=ssl_port, keepalive=60)#, bind_address=bind)

client.loop_forever()
