#!/usr/bin/python2
import paho.mqtt.client as mqtt
import time
import ssl


Topic = "grit_sub"


def on_connect(client, userdata, flags, rc):
    if rc == 0:
		print("Connected with result code "+ str(rc) + "\n")
		print("userdata "+str(userdata) + "\n")
		print("client "+str(client) + "\n")
		print("flags "+str(flags) + "\n")
		print("host " + client.get_host() + "\n")
    else:
		print("Bad connectio with result code "+ str(rc) + "\n")

def on_publish(client, userdata, mid):
    print("mid: "+str(mid))

#test_host = "iot.eclipse.org"
remote_host = "mqtt.grit.systems"
host = "localhost"
ssl_port = 8883

client = mqtt.Client("publisher_client")
client.on_connect = on_connect

print("Setting TLS")

ca_cert = "ca_certificate.pem"

client.tls_set(
	ca_certs=ca_cert,
	cert_reqs=ssl.CERT_NONE,
	tls_version=ssl.PROTOCOL_TLSv1_2)
#client.tls_insecure_set(True)

client.connect(host=remote_host, port=ssl_port, keepalive=60)
client.loop_start()

time.sleep(1)

while True:
    message = input("Enter message string to publish: >>> ")
    (rc, mid) = client.publish(Topic, message, qos=1)
    print("rc: " + str(rc))
    print("mid " + str(mid))
    time.sleep(1)
