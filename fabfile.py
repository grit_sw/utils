from time import sleep

from fabric.api import env, local, run, settings, sudo

# env.hosts = ['52.174.164.235']

# Uncomment for the production server
# env.hosts = ['api.gtg.grit.systems']
# env.user = 'root'

# # Uncomment for the staging server
env.hosts = ['dev-backend.gtg.grit.systems']
env.user = 'root'

# Uncomment for the docs server
# env.hosts = ['docs.gtg.grit.systems']
# env.user = 'root'

# Uncomment for the mqtt server
# env.hosts = ['mqtt.grit.systems']
# env.user = 'root'

#env.hosts = ['grit.systems']
#env.user = 'root'


def update():
	with settings(warn_only=True):
		sudo("apt-get update")


def git():
	sudo("apt-get install git -y")


def upstart():
	sudo("apt-get install upstart -y")


def docker_deps():
	# update()
	sudo('apt-get install linux-image-extra-$(uname -r) -y')
	sudo('apt-get install linux-image-extra-virtual -y')
	update()
	sudo('apt-get install apt-transport-https -y')
	sudo('apt-get install ca-certificates -y')
	sudo('apt-get install curl -y')
	sudo('apt-get install software-properties-common -y')
	run('curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -')
	sudo('add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"')
	update()


def install_docker():
	docker_deps()
	sudo('apt-get install docker-ce=18.03.0~ce-0~ubuntu -y')
	with settings(warn_only=True):
		sudo('groupadd docker')
	sudo('usermod -aG docker $USER')


def install_docker_compose():
	docker_deps()
	sudo('curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose')
	sudo('chmod +x /usr/local/bin/docker-compose')


def clone_ariaria():
	run("cd ~")
	with settings(warn_only=True):
		sudo("rm -rf ariaria")
	run("git clone git@bitbucket.org:dawnfuel/ariaria.git")
	run("cd ~/ariaria && git checkout refactor")


def clone_all():
	with settings(warn_only=True):
		sudo("rm -rf ~/gtg")
	run("cd ~ && mkdir gtg -p")
	clone_account_api()
	clone_auth_api()
	clone_buyorder_api()
	clone_energy_plan()
	clone_sellorder_api()
	clone_pin_api()
	clone_paystack_api()
	clone_prosumer_api()
	clone_subnet_api()
	clone_configuration_api()
	clone_users_api()
	clone_dev_docs()
	clone_utils()
	clone_cmdctrl()
	clone_sim_tx_engine()
	clone_mqtt_api()
	clone_feedback_api()
	clone_orderutils_api()
	clone_blockchain_api()
	clone_transaction_api()
	clone_txengine()

def clone_account_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/account_api.git")


def clone_auth_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/auth_api.git")


def clone_buyorder_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/buyorder_api.git")


def clone_energy_plan():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/energy_plan.git")


def clone_sellorder_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/sellorder_api.git")


def clone_orderutils_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/orderutils_api.git")


def clone_pin_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/pin_api.git")


def clone_paystack_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/paystack_api.git")


def clone_prosumer_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/prosumer_api.git")

def clone_subnet_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/subnet_api.git")

def clone_configuration_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/configuration_api.git")

def clone_users_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/users_api.git")


def clone_dev_docs():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/dev_docs.git")


def clone_utils():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/utils.git")

def clone_cmdctrl():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/cmdctrl.git")


def clone_sim_tx_engine():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/sim_tx_engine.git")


def clone_mqtt_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/mqtt_api.git")


def clone_feedback_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/feedback_api.git")


def clone_frontend():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/frontend.git")

def clone_blockchain_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/blockchain_api.git")

def clone_transaction_api():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/transaction_api.git")

def clone_txengine():
	run("cd ~/gtg && git clone git@bitbucket.org:grit_sw/gtg_tx_engine.git")


def pull_all():
	pull_account_api()
	pull_auth_api()
	pull_blockchain_api()
	pull_buyorder_api()
	pull_energy_plan()
	pull_sellorder_api()
	pull_pin_api()
	pull_paystack_api()
	pull_prosumer_api()
	pull_subnet_api()
	pull_configuration_api()
	pull_users_api()
	pull_dev_docs()
	pull_utils()
	pull_cmdctrl()
	pull_feedback_api()
	pull_sim_tx_engine()
	pull_blockchain_api()
	pull_transaction_api()
	pull_txengine()



def pull_account_api():
	run("cd ~/gtg/account_api && git stash && git checkout master && git pull")


def pull_blockchain_api():
	run("cd ~/gtg/blockchain_api && git stash && git checkout master && git pull")

def pull_transaction_api():
	run("cd ~/gtg/transaction_api && git stash && git checkout master && git pull")


def pull_auth_api():
	run("cd ~/gtg/auth_api && git stash && git checkout master && git pull")


def pull_buyorder_api():
	run("cd ~/gtg/buyorder_api && git stash && git checkout master && git pull")


def pull_energy_plan():
	run("cd ~/gtg/energy_plan && git stash && git checkout master && git pull")


def pull_sellorder_api():
	run("cd ~/gtg/sellorder_api && git stash && git checkout master && git pull")


def pull_orderutils_api():
	run("cd ~/gtg/orderutils_api && git stash && git checkout master && git pull")


def pull_pin_api():
	run("cd ~/gtg/pin_api && git stash && git checkout master && git pull")


def pull_paystack_api():
	run("cd ~/gtg/paystack_api && git stash && git checkout master && git pull")


def pull_prosumer_api():
	run("cd ~/gtg/prosumer_api && git stash && git checkout master && git pull")


def pull_subnet_api():
	run("cd ~/gtg/subnet_api && git stash && git checkout master && git pull")


def pull_configuration_api():
	run("cd ~/gtg/configuration_api && git stash && git checkout master && git pull")


def pull_users_api():
	run("cd ~/gtg/users_api && git stash && git checkout master && git pull")


def pull_dev_docs():
	run("cd ~/gtg/dev_docs && git stash && git checkout master && git pull")


def pull_utils():
	run("cd ~/gtg/utils && git stash && git checkout master && git pull")


def pull_cmdctrl():
	run("cd ~/gtg/cmdctrl && git stash && git checkout master && git pull")


def pull_sim_tx_engine():
	run("cd ~/gtg/sim_tx_engine && git stash && git checkout master && git pull")


def pull_mqtt_api():
	run("cd ~/gtg/mqtt_api && git stash && git checkout master && git pull")


def pull_feedback_api():
	run("cd ~/gtg/feedback_api && git stash && git checkout master && git pull")


def pull_frontend():
	run("cd ~/gtg/frontend && git stash && git checkout master && git pull")

def pull_txengine():
	run("cd ~/gtg/gtg_tx_engine && git stash && git checkout master && git pull")

def configure_prod():
	sudo("cp ~/gtg/utils/config_files/gtg_prod.conf /etc/init/gtg_prod.conf")


def configure_gtg():
	sudo("cp ~/gtg/utils/config_files/ariaria.conf /etc/init/ariaria.conf")
	sudo("cp ~/gtg/utils/config_files/swagger_compose.conf /etc/init/swagger_compose.conf")
	sudo("cp ~/gtg/utils/config_files/confluent.conf /etc/init/confluent.conf")



def create_images():
	with settings(warn_only=True):
		run("cd ~/gtg/account_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/auth_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/buyorder_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/energy_plan && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/sellorder_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/orderutils_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/pin_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/paystack_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/prosumer_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/subnet_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/configuration_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/users_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/feedback_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/dev_docs && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/cmdctrl && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/utils/database && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/utils/redis && chmod +x 'build.sh' && ./build.sh")
		build_haproxy_main_staging()
		build_haproxy_main_production()
		run("cd ~/gtg/utils/haproxy_doc && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/utils/haproxy_auth && chmod +x 'build.sh' && ./build.sh")
		build_simulator()
		run("cd ~/gtg/mqtt_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/blockchain_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/transaction_api && chmod +x 'build.sh' && ./build.sh")
		run("cd ~/gtg/gtg_tx_engine && chmod +x 'build.sh' && ./build.sh")


def create_db():
	with settings(warn_only=True):
		run("cd ~/gtg/utils/database && chmod +x 'build.sh' && ./build.sh")

def build_haproxy_main_staging():
	run("cd ~/gtg/utils/haproxy_main_staging && chmod +x 'build.sh' && ./build.sh")

def build_haproxy_main_production():
	run("cd ~/gtg/utils/haproxy_main_production && chmod +x 'build.sh' && ./build.sh")

def build_simulator():
	run("cd ~/gtg/sim_tx_engine && chmod +x 'build.sh' && ./build.sh")
	run("cd ~/gtg/sim_tx_engine && chmod +x 'build_api.sh' && ./build_api.sh")
	run("cd ~/gtg/sim_tx_engine && chmod +x 'build_listener.sh' && ./build_listener.sh")
	run("cd ~/gtg/sim_tx_engine && chmod +x 'build_consumer.sh' && ./build_consumer.sh")
	run("cd ~/gtg/sim_tx_engine && chmod +x 'build_producer.sh' && ./build_producer.sh")

def create_volumes():
	run('docker volume create --name=users_data')
	run('docker volume create --name=account_data')
	run('docker volume create --name=pins_data')
	run('docker volume create --name=paystack_data')
	run('docker volume create --name=auth_data')
	run('docker volume create --name=energyplan_data')
	run('docker volume create --name=prosumer_data')
	run('docker volume create --name=subnet_data')
	run('docker volume create --name=configuration_data')
	run('docker volume create --name=cmdctrl_data')
	run('docker volume create --name=stream_data')
	run('docker volume create --name=buy_order_data')
	run('docker volume create --name=order_utils_data')
	run('docker volume create --name=feedback_data')
	run('docker volume create --name=sell_order_data')
	run('docker volume create --name=tx_engine_data')
	run('docker volume create --name=transaction_data')
	run('docker volume create --name=sim_tx_data')
	# create_doc_volumes()


def create_doc_volumes():
	run('docker volume create --name=doc_users_data')
	run('docker volume create --name=doc_account_data')
	run('docker volume create --name=doc_pins_data')
	run('docker volume create --name=doc_paystack_data')
	run('docker volume create --name=doc_auth_data')
	run('docker volume create --name=doc_energyplan_data')
	run('docker volume create --name=doc_prosumer_data')
	run('docker volume create --name=doc_subnet_data')
	run('docker volume create --name=doc_configuration_data')
	run('docker volume create --name=doc_cmdctrl_data')
	run('docker volume create --name=doc_auth_data')
	run('docker volume create --name=doc_buy_order_data')
	run('docker volume create --name=doc_sell_order_data')
	run('docker volume create --name=doc_order_utils_data')
	run('docker volume create --name=doc_feedback_data')
	run('docker volume create --name=doc_tx_engine_data')
	run('docker volume create --name=doc_mongo_data')
	run('docker volume create --name=doc_transaction_data')
	run('docker volume create --name=doc_sim_tx_data')


def drop_doc_volumes():
	with settings(warn_only=True):
		run('docker volume rm doc_users_data')
		run('docker volume rm doc_account_data')
		run('docker volume rm doc_pins_data')
		run('docker volume rm doc_paystack_data')
		run('docker volume rm doc_auth_data')
		run('docker volume rm doc_energyplan_data')
		run('docker volume rm doc_prosumer_data')
		run('docker volume rm doc_subnet_data')
		run('docker volume rm doc_configuration_data')
		run('docker volume rm doc_cmdctrl_data')
		run('docker volume rm doc_auth_data')
		run('docker volume rm doc_buy_order_data')
		run('docker volume rm doc_sell_order_data')
		run('docker volume rm doc_order_utils_data')
		run('docker volume rm doc_feedback_data')
		run('docker volume rm doc_mongo_data')
		run('docker volume rm doc_transaction_data')


def recreate_doc_volumes():
	drop_doc_volumes()
	create_doc_volumes()


def volumes():
	run('docker volume ls')


def drop_volumes():
	run('docker volume rm $(docker volume ls -q)')


def drop_users():
	with settings(warn_only=True):
		run('docker volume rm users_data')
		run('docker volume rm doc_users_data')
		run('docker volume create --name=users_data')
		run('docker volume create --name=doc_users_data')


def drop_buyorders():
	with settings(warn_only=True):
		run('docker volume rm buy_order_data')
		run('docker volume rm doc_buy_order_data')
		run('docker volume create --name=buy_order_data')
		run('docker volume create --name=doc_buy_order_data')


def drop_prosumer_db():
	with settings(warn_only=True):
		run('docker volume rm prosumer_data')
		run('docker volume rm doc_prosumer_data')
		run('docker volume create --name=prosumer_data')
		run('docker volume create --name=doc_prosumer_data')


def drop_cmdctrl():
	with settings(warn_only=True):
		run('docker volume rm cmdctrl_data')
		run('docker volume rm stream_data')
		run('docker volume create --name=cmdctrl_data')
		run('docker volume create --name=stream_data')


def drop_subnet_db():
	with settings(warn_only=True):
		run('docker volume rm subnet_data')
		run('docker volume rm doc_subnet_data')
		run('docker volume create --name=subnet_data')
		run('docker volume create --name=doc_subnet_data')


def drop_configuration_db():
	with settings(warn_only=True):
		run('docker volume rm configuration_data')
		run('docker volume rm doc_configuration_data')
		run('docker volume create --name=configuration_data')
		run('docker volume create --name=doc_configuration_data')


def drop_order_db():
	with settings(warn_only=True):
		run('docker volume rm buy_order_data')
		run('docker volume rm sell_order_data')
		run('docker volume rm order_utils_data')
		run('docker volume rm doc_tx_engine_data')
		run('docker volume rm doc_order_utils_data')
		run('docker volume create --name=buy_order_data')
		run('docker volume create --name=sell_order_data')
		run('docker volume create --name=order_utils_data')
		run('docker volume create --name=doc_tx_engine_data')
		run('docker volume create --name=doc_order_utils_data')


def drop_doc_order_db():
	with settings(warn_only=True):
		run('docker volume rm doc_tx_engine_data')
		run('docker volume rm doc_order_utils_data')
		run('docker volume create --name=doc_tx_engine_data')
		run('docker volume create --name=doc_order_utils_data')


def drop_account():
	run('docker volume rm account_data')
	run('docker volume rm doc_account_data')
	run('docker volume create --name=account_data')
	run('docker volume create --name=doc_account_data')


def drop_pins():
	run('docker volume rm pins_data')
	run('docker volume rm doc_pins_data')
	run('docker volume create --name=pins_data')
	run('docker volume create --name=doc_pins_data')

def drop_transaction_db():
	with settings(warn_only=True):
		run('docker volume rm transaction_data')
		run('docker volume rm doc_transaction_data')
		run('docker volume create --name=transaction_data')
		run('docker volume create --name=doc_transaction_data')


def drop_sim_tx_db():
	with settings(warn_only=True):
		run('docker volume rm sim_tx_data')
		run('docker volume rm doc_sim_tx_data')
		run('docker volume create --name=sim_tx_data')
		run('docker volume create --name=doc_sim_tx_data')


def prune_images():
	run('docker rmi -f $(docker images --filter "dangling=true" -q)')


def remove_all():
	run('docker stop $(docker ps -aq)')
	run('docker rm $(docker ps -aq)')


def images():
	run('docker images')


def services():
	run('docker ps')


def logs():
	run('cd ~/gtg/utils && docker-compose logs -f users_api')


def log_error():
	run('cd ~/gtg/utils && docker-compose logs -f | grep exception')


def restart_service():
	run('cd ~/gtg/utils && docker-compose stop proxy && docker-compose start proxy')

def log_service():
	# run('cd ~/gtg/utils && docker-compose logs -f order_utils_api sellorder_api buyorder_api')
	# run('cd ~/gtg/utils && docker-compose logs -f feedback_api')
	# run('cd ~/gtg/utils && docker-compose logs -f configuration_api prosumer_api  account_api  paystack_api cmdctrl_online')
	# run('cd ~/gtg/utils && docker-compose logs -f account_api configuration_api paystack_api users_api')
	# run('cd ~/gtg/utils && docker-compose logs -f order_utils_api sellorder_api buyorder_api cmdctrl_online')
	# run('cd ~/gtg/utils && docker-compose logs -f sellorder_api buyorder_api')
	# run('cd ~/gtg/utils && docker-compose logs -f prosumer_api cmdctrl_online account_api subnet_api configuration_api')
	# run('cd ~/gtg/utils && docker-compose logs -f prosumer_api configuration_api')
	# run('cd ~/gtg/utils && docker-compose logs -f buyorder_api sellorder_api')
	# run('cd ~/gtg/utils && docker-compose logs -f pin_api paystack_api mqtt_api prosumer_api cmdctrl_online')
	# run('cd ~/gtg/utils && docker-compose logs -f pin_api mqtt_api paystack_api')
	run('cd ~/gtg/utils && docker-compose logs -f auth_api users_api')
	# run('cd ~/gtg/utils && docker-compose logs -f tx_listener electricity_consumer electricity_producer')
	# run('cd ~/gtg/utils && docker-compose logs -f auth_api users_api cmdctrl_online cmdctrl_api')
	# run('cd ~/gtg/utils && docker-compose logs -f mqtt_api')


def stop_service():
	run('cd ~/gtg/utils && docker-compose -f production.yml stop cmdctrl_online')


def start_service():
	run('cd ~/gtg/utils && docker-compose -f production.yml start cmdctrl_online')


def doc_logs():
	# run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml logs --tail="all" ')
	# run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml logs -f ')
	run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml logs -f doc_auth_api doc_blockchain_api')


def doc_log_service():
	# run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml logs --tail="all" ')
	# run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml logs -f ')
	run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml logs -f doc_sellorder_api')


def doc_log_simulator():
	run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml logs -f doc_simulator_api')


def log_size():
	sudo('sh -c "du -ch /var/lib/docker/containers/*/*-json.log"')


def delete_logs():
	sudo('sh -c "truncate -s 0 /var/lib/docker/containers/*/*-json.log"')


def deploy_confluent():
	with settings(warn_only=True):
		pull_utils()
		configure_gtg()


def ps():
	with settings(warn_only=True):
		run('docker ps')


def network_ls():
	with settings(warn_only=True):
		run('docker network ls')


def network_prune():
	with settings(warn_only=True):
		run('docker network prune')


def start_confluent():
	with settings(warn_only=True):
		sudo('service confluent start')


def stop_confluent():
	with settings(warn_only=True):
		sudo('service confluent stop')
		run('cd ~/gtg/utils/confluent && docker-compose down')


def restart_confluent():
	with settings(warn_only=True):
		stop_confluent()
		start_confluent()


def start_prod():
	# sudo('service gtg_prod start')
	run('cd ~/gtg/utils && docker-compose -f production.yml up --build -d')


def start_main():
	sudo('cd ~/gtg/utils/ && docker-compose up --build -d')


def start_docs():
	sudo('service swagger_compose start')


def start_mqtt_server():
	run('cd ~/gtg/utils/vernemq && docker-compose -f mqtt-compose.yml up --build -d')


def status():
	sudo('service ariaria status')
	sudo('service swagger_compose status')


# def restart():
#     stop()
#     start_main()


def stop():
	with settings(warn_only=True):
		sudo('service ariaria stop')
		sudo('service swagger_compose stop')
		run('cd ~/gtg/utils && docker-compose stop')
		run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml stop')


def down():
	with settings(warn_only=True):
		sudo('service ariaria stop')
		# sudo('service swagger_compose stop')
		run('cd ~/gtg/utils && docker-compose down')
		# run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml down')


def down_main():
	with settings(warn_only=True):
		# sudo('service ariaria stop')
		# sudo('service swagger_compose stop')
		run('cd ~/gtg/utils && docker-compose down')
		# run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml down')


def down_prod():
	with settings(warn_only=True):
		sudo('service gtg_prod stop')
		run('cd ~/gtg/utils && docker-compose -f production.yml down')


def down_docs():
	with settings(warn_only=True):
		sudo('service swagger_compose stop')
		run('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml down')


def stop_account():
	with settings(warn_only=True):
		run('cd ~/gtg/utils && docker-compose stop account_api')


def stop_users():
	with settings(warn_only=True):
		run('cd ~/gtg/utils && docker-compose stop users_api')


def start_users():
	with settings(warn_only=True):
		run('cd ~/gtg/utils && docker-compose start users_api')


def restart():
	with settings(warn_only=True):
		down()
		start_main()


def restart_main():
	with settings(warn_only=True):
		sudo('service ariaria stop')
		sudo('cd ~/gtg/utils && docker-compose down')
		rebuild()


def restart_prod():
	with settings(warn_only=True):
		sudo('service gtg_prod stop')
		down_prod()
		sudo('service gtg_prod start')


def restart_docs():
	with settings(warn_only=True):
		sudo('service swagger_compose stop')
		sudo('service swagger_compose start')


def rebuild():
	with settings(warn_only=True):
		# start_confluent()
		pull_utils()
		sudo('cd ~/gtg/utils && docker-compose up -d --build')


def rebuild_docs():
	with settings(warn_only=True):
		sudo('cd ~/gtg/utils && docker-compose -f docker-compose-docs.yml up -d --build')


def rebuild_prod():
	with settings(warn_only=True):
		sudo('cd ~/gtg/utils && docker-compose -f production.yml up -d --build')

def rebuild_confluent():
	with settings(warn_only=True):
		sudo('cd ~/gtg/utils/confluent && docker-compose up -d --build')


def redeploy_confluent():
	with settings(warn_only=True):
		pull_utils()
		sudo('cd ~/gtg/utils/confluent && docker-compose up -d --build')


def build_utils():
	run("cd ~/gtg/utils/database && chmod +x 'build.sh' && ./build.sh")
	run("cd ~/gtg/utils/redis && chmod +x 'build.sh' && ./build.sh")
	run("cd ~/gtg/utils/haproxy && chmod +x 'build.sh' && ./build.sh")
	run("cd ~/gtg/utils/haproxy_doc && chmod +x 'build.sh' && ./build.sh")
	run("cd ~/gtg/utils/haproxy_auth && chmod +x 'build.sh' && ./build.sh")


def redeploy():
	with settings(warn_only=True):
		# clone_all()
		pull_all()
		create_images()
		create_volumes()
		# create_doc_volumes()
		redeploy_confluent()
		sleep(20)  # So Kakfa services can start up
		rebuild()
		prune_images()


def redeploy_prod():
	with settings(warn_only=True):
		# clone_all()
		pull_all()
		create_images()
		rebuild_prod()
		prune_images()


def redeploy_docs():
	with settings(warn_only=True):
		# clone_all()
		pull_all()
		create_images()
		down_docs()
		recreate_doc_volumes()
		rebuild_docs()
		prune_images()


def redeploy_frontend():
	with settings(warn_only=True):
		# clone_all()
		pull_frontend()
		run("cd ~/gtg/frontend && chmod +x 'deploy_stage.sh' && ./deploy_stage.sh")
		rebuild()
		prune_images()


def redeploy_pin_api():
	with settings(warn_only=True):
		# clone_all()
		pull_pin_api()
		run("cd ~/gtg/pin_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_subnet_api():
	with settings(warn_only=True):
		# clone_all()
		pull_subnet_api()
		run("cd ~/gtg/subnet_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_account_api():
	with settings(warn_only=True):
		# clone_all()
		pull_account_api()
		run("cd ~/gtg/account_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_paystack_api():
	with settings(warn_only=True):
		# clone_all()
		pull_paystack_api()
		run("cd ~/gtg/paystack_api && chmod +x 'build.sh' && ./build.sh")
		rebuild_prod()
		prune_images()


def redeploy_prosumer_api():
	with settings(warn_only=True):
		# clone_all()
		pull_prosumer_api()
		run("cd ~/gtg/prosumer_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_configuration_api():
	with settings(warn_only=True):
		# clone_all()
		pull_configuration_api()
		run("cd ~/gtg/configuration_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_blockchain_api():
	with settings(warn_only=True):
		pull_blockchain_api()
		run("cd ~/gtg/blockchain_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()

def redeploy_transaction_api():
	with settings(warn_only=True):
		pull_transaction_api()
		run("cd ~/gtg/transaction_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()

def redeploy_txengine():
	with settings(warn_only=True):
		pull_txengine()
		run("cd ~/gtg/gtg_tx_engine && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_mqtt_api():
	with settings(warn_only=True):
		# clone_all()
		pull_mqtt_api()
		run("cd ~/gtg/mqtt_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_feedback_api():
	with settings(warn_only=True):
		# clone_all()
		pull_feedback_api()
		run("cd ~/gtg/feedback_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_cmdctrl():
	with settings(warn_only=True):
		# clone_all()
		pull_cmdctrl()
		run("cd ~/gtg/cmdctrl && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_cmdctrl_api():
	with settings(warn_only=True):
		# clone_all()
		pull_cmdctrl()
		run("cd ~/gtg/cmdctrl && chmod +x 'build_api.sh' && ./build_api.sh")
		rebuild()
		prune_images()


def redeploy_cmdctrl_online():
	with settings(warn_only=True):
		# clone_all()
		pull_cmdctrl()
		run("cd ~/gtg/cmdctrl && chmod +x 'build_online.sh' && ./build_online.sh")
		rebuild()
		prune_images()


def redeploy_cmdctrl_offline():
	with settings(warn_only=True):
		# clone_all()
		pull_cmdctrl()
		run("cd ~/gtg/cmdctrl && chmod +x 'build_offline.sh' && ./build_offline.sh")
		rebuild()
		prune_images()


def redeploy_simulator():
	with settings(warn_only=True):
		# clone_all()
		pull_sim_tx_engine()
		build_simulator()
		rebuild()
		prune_images()


def restart_users_api():
	with settings(warn_only=True):
		run('cd ~/gtg/utils && docker-compose restart users_api')


def restart_paystack_api():
	with settings(warn_only=True):
		run('cd ~/gtg/utils && docker-compose restart paystack_api')


def restart_account_api():
	with settings(warn_only=True):
		run('cd ~/gtg/utils && docker-compose restart account_api')


def restart_cmdctrl_online():
	with settings(warn_only=True):
		run('cd ~/gtg/utils && docker-compose restart cmdctrl_online')


def redeploy_users_api():
	with settings(warn_only=True):
		# clone_all()
		pull_users_api()
		run("cd ~/gtg/users_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		# rebuild_prod()
		prune_images()


def redeploy_buyorder_api():
	with settings(warn_only=True):
		# clone_all()
		pull_buyorder_api()
		run("cd ~/gtg/buyorder_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_orderutils_api():
	with settings(warn_only=True):
		# clone_all()
		pull_orderutils_api()
		run("cd ~/gtg/orderutils_api && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()


def redeploy_sellorder_api():
	with settings(warn_only=True):
		# clone_all()
		pull_sellorder_api()
		run("cd ~/gtg/sellorder_api && chmod +x 'build.sh' && ./build.sh")
		# rebuild_docs()
		rebuild()
		prune_images()


def redeploy_utils():
	with settings(warn_only=True):
		# clone_all()
		pull_utils()
		build_utils()
		rebuild()
		prune_images()


def redeploy_auth():
	with settings(warn_only=True):
		# clone_all()
		pull_auth_api()
		run("cd ~/gtg/auth_api && chmod +x 'build.sh' && ./build.sh")
		rebuild_prod()
		prune_images()


def redeploy_energy_plan():
	with settings(warn_only=True):
		# clone_all()
		pull_energy_plan()
		run("cd ~/gtg/energy_plan && chmod +x 'build.sh' && ./build.sh")
		rebuild()
		prune_images()

def install_server_requirements():
	with settings(warn_only=True):
		# update()
		git()
		upstart()
		install_docker()
		install_docker_compose()

def deploy_mqtt():
	with settings(warn_only=True):
		install_server_requirements()
		sudo("rm -rf ~/gtg")
		run("cd ~ && mkdir gtg -p")
		clone_utils()
		start_mqtt_server()


def deploy():
	with settings(warn_only=True):
		install_server_requirements()
		clone_all()
		configure_gtg()
		create_images()
		create_volumes()
		start_main()


def deploy_docs():
	with settings(warn_only=True):
		install_server_requirements()
		clone_all()
		sudo("cp ~/gtg/utils/config_files/swagger_compose.conf /etc/init/swagger_compose.conf")
		create_images()
		drop_doc_volumes()
		create_doc_volumes()
		rebuild_docs()
		prune_images()


def deploy_prod():
	with settings(warn_only=True):
		install_server_requirements()
		clone_all()
		configure_prod()
		create_images()
		create_volumes()
		start_prod()


"""
Local(not meant for server) configurations
"""

def local_pull_all():
	local_pull_account_api()
	local_pull_auth_api()
	local_pull_buyorder_api()
	local_pull_energy_plan()
	local_pull_sellorder_api()
	local_pull_pin_api()
	local_pull_paystack_api()
	local_pull_prosumer_api()
	local_pull_users_api()
	local_pull_dev_docs()
	local_pull_utils()
	local_pull_cmdctrl()
	local_pull_sim_tx_engine()


def local_pull_account_api():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/account_api && git stash && git checkout master && git pull && git checkout dev")


def local_pull_auth_api():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/auth_api && git stash && git checkout master && git pull && git checkout dev")


def local_pull_buyorder_api():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/buyorder_api && git stash && git checkout master && git pull && git checkout dev")


def local_pull_energy_plan():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/energy_plan && git stash && git checkout master && git pull && git checkout dev")


def local_pull_sellorder_api():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/sellorder_api && git stash && git checkout master && git pull && git checkout dev")


def local_pull_pin_api():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/pin_api && git stash && git checkout master && git pull && git checkout dev")


def local_pull_paystack_api():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/paystack_api && git stash && git checkout master && git pull && git checkout dev")


def local_pull_prosumer_api():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/prosumer_api && git stash && git checkout master && git pull && git checkout dev")


def local_pull_users_api():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/users_api && git stash && git checkout master && git pull && git checkout dev")


def local_pull_dev_docs():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/dev_docs && git stash && git checkout master && git pull && git checkout dev")


def local_pull_utils():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/utils && git stash && git checkout master && git pull && git checkout dev")


def local_pull_cmdctrl():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/cmdctrl && git stash && git checkout master && git pull && git checkout dev")


def local_pull_sim_tx_engine():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/sim_tx_engine && git stash && git checkout master && git pull && git checkout dev")


def local_create_images():
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/account_api && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/auth_api && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/buyorder_api && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/energy_plan && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/sellorder_api && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/pin_api && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/paystack_api && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/prosumer_api && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/users_api && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/dev_docs && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/cmdctrl && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/utils/database && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/utils/redis && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/utils/haproxy && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/utils/haproxy_doc && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/utils/haproxy_auth && chmod +x build.sh && ./build.sh")
	local("cd /home/moyo/Documents/REPOS/01-Projects/GTG/sim_tx_engine && chmod +x build.sh && ./build.sh")


def local_create_volumes():
	local('docker volume create --name=users_data')
	local('docker volume create --name=account_data')
	local('docker volume create --name=pins_data')
	local('docker volume create --name=paystack_data')
	local('docker volume create --name=auth_data')
	local('docker volume create --name=energyplan_data')
	local('docker volume create --name=prosumer_data')
	local('docker volume create --name=subnet_data')
	local('docker volume create --name=configuration_data')
	local('docker volume create --name=cmdctrl_data')
	local('docker volume create --name=stream_data')
	local('docker volume create --name=buy_order_data')
	local('docker volume create --name=order_utils_data')
	local('docker volume create --name=feedback_data')
	local('docker volume create --name=sell_order_data')
	local('docker volume create --name=tx_engine_data')
	local('docker volume create --name=transaction_data')
	local('docker volume create --name=sim_tx_data')


def local_create_doc_volumes():
	local('docker volume create --name=doc_users_data')
	local('docker volume create --name=doc_account_data')
	local('docker volume create --name=doc_pins_data')
	local('docker volume create --name=doc_paystack_data')
	local('docker volume create --name=doc_sell_order_data')
	local('docker volume create --name=doc_buy_order_data')
	local('docker volume create --name=doc_auth_data')
	local('docker volume create --name=doc_energyplan_data')
	local('docker volume create --name=doc_prosumer_data')
	local('docker volume create --name=doc_cmdctrl_data')
	local('docker volume create --name=doc_tx_engine_data')


def redeploy_local():
	local_confluent()
	local_pull_all()
	local_create_volumes()
	local_create_images()
	local_rebuild()


def local_drop_order_db():
	local('docker volume rm buy_order_data')
	local('docker volume rm sell_order_data')
	local('docker volume create --name=buy_order_data')
	local('docker volume create --name=sell_order_data')


def local_confluent():
	with settings(warn_only=True):
		local('cd /home/moyo/Documents/REPOS/01-Projects/GTG/utils/confluent && docker-compose up -d --build')


def local_rebuild():
	with settings(warn_only=True):
		local('cd /home/moyo/Documents/REPOS/01-Projects/GTG/utils && docker-compose up -d --build')


def local_clone_all():
	# with settings(warn_only=True):
		# sudo("rm -rf ~/gtg")
	# run("cd ~ && mkdir gtg -p")
	local_clone_account_api()
	local_clone_auth_api()
	local_clone_buyorder_api()
	local_clone_energy_plan()
	local_clone_sellorder_api()
	local_clone_pin_api()
	local_clone_paystack_api()
	local_clone_prosumer_api()
	local_clone_subnet_api()
	local_clone_configuration_api()
	local_clone_users_api()
	local_clone_dev_docs()
	# local_clone_utils()
	local_clone_cmdctrl()
	local_clone_sim_tx_engine()
	local_clone_mqtt_api()
	local_clone_orderutils_api()
	local_clone_blockchain_api()
	local_clone_transaction_api()

def local_clone_account_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/account_api.git")


def local_clone_auth_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/auth_api.git")


def local_clone_buyorder_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/buyorder_api.git")


def local_clone_energy_plan():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/energy_plan.git")


def local_clone_sellorder_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/sellorder_api.git")


def local_clone_orderutils_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/orderutils_api.git")


def local_clone_pin_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/pin_api.git")


def local_clone_paystack_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/paystack_api.git")


def local_clone_prosumer_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/prosumer_api.git")

def local_clone_subnet_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/subnet_api.git")

def local_clone_configuration_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/configuration_api.git")

def local_clone_users_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/users_api.git")


def local_clone_dev_docs():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/dev_docs.git")


# def local_clone_utils():
# 	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/utils.git")

def local_clone_cmdctrl():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/cmdctrl.git")


def local_clone_sim_tx_engine():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/sim_tx_engine.git")


def local_clone_mqtt_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/mqtt_api.git")


def local_clone_feedback_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/feedback_api.git")


def local_clone_frontend():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/frontend.git")

def local_clone_blockchain_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/blockchain_api.git")

def local_clone_transaction_api():
	local("cd ~/Documents/REPOS/01-Projects/GTG && git clone git@bitbucket.org:grit_sw/transaction_api.git")
