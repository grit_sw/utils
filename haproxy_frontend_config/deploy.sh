#!/bin/bash

cd /root/frontend_apps/configuration_frontend && 
  git checkout master && 
  git pull &&

cd /root/frontend_apps/gtg_frontend && 
  git checkout master && 
  git pull &&

cd /root/frontend_apps/gtg_yc_demo && 
  git checkout yc-demo && 
  git pull &&

cd /root/frontend_apps/cmdctrl_frontend && 
  git checkout master && 
  git pull &&

cd /root/frontend_apps/water_metering && 
  git checkout water-meter-demo && 
  git pull &&

cd /root/frontend_apps/dashboard_graphs && 
  git checkout master && 
  git pull &&

cd /root/frontend_apps/utils/haproxy_frontend_config && 
  git checkout master && 
  git pull &&
  docker-compose -f production.yml up --build -d &&


  docker-compose -f production.yml restart haproxy
