#!/bin/bash

cd /root/frontend_apps/dashboard_graphs && 
  git checkout master && 
  git pull &&

cd /root/frontend_apps/utils/haproxy_frontend_config && 
  git checkout master && 
  git pull &&
  docker-compose -f production.yml up --build -d &&


  docker-compose -f production.yml restart haproxy
